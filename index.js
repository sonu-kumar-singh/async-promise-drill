const fs = require('fs');
const fsPromise = fs.promises;




async function login (username, password){
    let flag = false;
    let data = ''
    let userInfo = {};
await fsPromise.readFile('./login.json', {encoding: 'utf-8'})
.then(res => {
    data = JSON.parse(res);
    if(JSON.parse(res).hasOwnProperty(username) && JSON.parse(res)[username] === password){
           flag = true;
    }
}, rej => {
    console.log('error occured while reading login.json file', rej);
})
if(flag){
    userInfo.username = username;
    userInfo.uniqueId = username + password;
    return  Promise.resolve(userInfo);
}else{
    if(!data.hasOwnProperty(username)){
        return Promise.reject({'user dont exist': 404});
    }
    
    else if(data[username] !== password){
        return Promise.reject({'password is incorrect': 400});
    }
   
    else{
        return Promise.reject({'Internal server error': 500});
    }
   
}
}

async function getUserProfile (user) {
    let data = '';
    await fsPromise.readFile('./profile.json', {encoding: 'utf-8'})
    .then((res) => data = JSON.parse(res), 
    (rej) => console.log('error occured in reading profile.json file', rej))
    if(data.hasOwnProperty(user))
    return Promise.resolve(data[user]);
    else{
        if(!data.hasOwnProperty(user))
        return Promise.reject({'user not found': 404});
        else
        return Promise.reject({'Internal Server Error': 500});
    }
}

async function getToDoList (user) {
    let data = ''
    await fsPromise.readFile('./todo-list.json', {encoding: 'utf-8'})
         .then((res) => data = JSON.parse(res),
         (rej) => console.log('error occured while reading todolist.json file', rej))
     if(data.hasOwnProperty(user))
     return Promise.resolve(data[user]);
     else
     return Promise.reject({'Internal Server Error': 500});
}

function expireUniqueId (userInfo) {
 setTimeout(() => {
 userInfo.uniqueId = undefined;
 },120000)
}






login("jim@microsoft.com", "jimmarketplace")
.then(res => {return res})
.catch(rej => {throw rej})
.then(res => {
    return getUserProfile(res.username)
    expireUniqueId(res).then((res) => {
        if(res.uniqueId === undefined)
        throw new Error('uniqueId is expired');
    }).catch(rej => {throw rej})
}, rej => {return rej})
.then(res => console.log(res), rej=> console.log(rej))



login("walker@discord.com", "walker-5")
.then(res => {return res})
.catch(rej => {throw rej})
.then(res => {
    return getToDoList(res.username)
    expireUniqueId(res).then((res) => {
        if(res.uniqueId === undefined)
        throw new Error('uniqueId is expired');
    }).catch(rej => {throw rej})
}, rej => {return rej})
.then(res => console.log(res), rej=> console.log(rej))











